//
//  ViewController.swift
//  AlamoFireSwiftyJSONObjectMapper
//
//  Created by BS23 on 5/28/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class ViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, RequestManagerDelegate {
    
    @IBOutlet weak var dfdf: UITableView!
    
    
    var users = [User]()
    var isSuccess : Bool = false
    var spinningActivity : MBProgressHUD?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        spinningActivity = MBProgressHUD.showAdded(to: self.view, animated: true)
        spinningActivity?.label.text = "...Loading..."
        spinningActivity?.detailsLabel.text = "Keep wait."
        //loading?.mode = .determinateHorizontalBar
        spinningActivity?.isUserInteractionEnabled = false
        
        
        
        RequestManager.sharedInstance.getData()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        RequestManager.sharedInstance.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        RequestManager.sharedInstance.delegate = nil
    }
    
    
    
    func getUserData(data: [User], success: Bool)
    {
        
        users = data
        isSuccess = success
        if (isSuccess)
        {
            self.spinningActivity?.hide(animated: true)
        }
        self.dfdf.reloadData()

    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.users.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        as! CustomeTableViewCell
        
        cell.id.text = users[indexPath.row].id
        cell.email.text = users[indexPath.row].email
        cell.name.text = users[indexPath.row].name
        cell.gender.text = users[indexPath.row].gender
        cell.address.text = users[indexPath.row].address
        
        cell.mobile.text = users[indexPath.row].phone.mobile
        cell.home.text = users[indexPath.row].phone.home
        cell.office.text = users[indexPath.row].phone.office
        
        return cell
    }
}

