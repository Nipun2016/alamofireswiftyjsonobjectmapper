//
//  CustomeTableViewCell.swift
//  AlamoFireSwiftyJSONObjectMapper
//
//  Created by BS23 on 5/28/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit

class CustomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var gender: UILabel!
    
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var home: UILabel!
    @IBOutlet weak var office: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
