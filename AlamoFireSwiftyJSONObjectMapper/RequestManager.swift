//
//  RequestManager.swift
//  AlamoFireSwiftyJSONObjectMapper
//
//  Created by BS23 on 5/29/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


// Delegate

protocol RequestManagerDelegate {
    
    func getUserData(data: [User], success: Bool)
}


class RequestManager: NSObject {
    
    //SingleTon
    
    static let sharedInstance: RequestManager = {
    
        let instance = RequestManager()
        
        return instance
    }()
    
    
    
    
    var delegate : RequestManagerDelegate? = nil
    
    var users = [User]()
    
    func getData()
    {
        
        var myResponse : JSON = JSON.null
     
        let url = URL (string: "http://api.androidhive.info/contacts/")
        var request = URLRequest(url : url! as URL)
        request.httpMethod = "GET"
        
        
        Alamofire.request(request).responseJSON { responds in
            
            switch responds.result {
                
            case .success(let data):
                
                print("Success \(data)")
                
                myResponse = JSON(data)
                
                for i in 0..<myResponse["contacts"].count
                {
                    let singleUser = User(UserJSON: myResponse["contacts"][i])
                    self.users.append(singleUser)
                }
                
                self.delegate?.getUserData(data: self.users, success: true)

            case .failure(let error):
                print("Error \(error)")
            }
        }
    }
}
